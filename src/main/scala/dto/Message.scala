package dto

import java.util.Date

case class Message(subject: String, from: String, replyTo: String, body: String, date: Date)
