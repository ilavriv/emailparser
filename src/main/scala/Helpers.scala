import java.io.File

object Helpers {
  def listFiles(path: String): List[File] =
    Some(new File(path))
      .filter(f => f.exists && f.isDirectory)
      .toList
      .flatMap(_.listFiles)
      .filter(_.isFile)
}
