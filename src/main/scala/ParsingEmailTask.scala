import org.simplejavamail.outlookmessageparser.OutlookMessageParser
import org.simplejavamail.outlookmessageparser.model.OutlookMessage
import java.io.File
import java.util.concurrent.RecursiveTask


case class ParsingEmailTask(data: List[File]) extends RecursiveTask[List[dto.Message]] {
  final val BATCH_SIZE = 100

  def apply(): List[dto.Message] = compute()

  override def compute(): List[dto.Message] = if(data.length < BATCH_SIZE) {
    data.map(parseEmail)
  } else {
    data.grouped(data.length / 2)
      .map(ParsingEmailTask)
      .map(_.fork())
      .flatMap(_.join())
      .toList
  }

  def parseEmail(file: File): dto.Message = Some(new OutlookMessageParser())
      .map(_.parseMsg(file))
      .map(mapMessage)
      .get

  def mapMessage(m: OutlookMessage): dto.Message = dto.Message(
    m.getSubject,
    m.getFromEmail,
    m.getReplyToEmail,
    m.getBodyText,
    m.getDate
  )
}
