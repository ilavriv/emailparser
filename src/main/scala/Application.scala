import java.util.concurrent.ForkJoinPool

object Application extends App {
  import Helpers._

  final val DATASET_PATH = "/Users/ilavriv/data/Backups/Polly"
  val files = listFiles(DATASET_PATH)
  val pool = new ForkJoinPool()
  val messages = pool.invoke(ParsingEmailTask(files))

  messages.foreach(println)
}