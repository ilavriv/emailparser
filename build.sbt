name := "Collectable"

version := "0.1"

scalaVersion := "2.13.7"

val kafkaVersion = "2.3.0"

libraryDependencies ++= Seq(
  "org.simplejavamail" % "outlook-message-parser" % "1.7.13",
  "org.apache.kafka" % "kafka-streams" % kafkaVersion,
  "org.apache.kafka" %% "kafka-streams-scala" % kafkaVersion
)